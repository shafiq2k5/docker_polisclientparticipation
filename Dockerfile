FROM centos:7
MAINTAINER uzzal, uzzal2k5@gmail.com
WORKDIR /polisClien
RUN yum install -y net-tools
# replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
# update the repository sources list
# and install dependencies
RUN yum install -y git
# nvm environment variables
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 6.11.1
# install nvm
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.25.0/install.sh | bash
# install node and npm
RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default
# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# confirm installation

RUN node -v
RUN npm -v
# Source Code down fron git
RUN npm install -g bower
RUN git clone  https://github.com/pol-is/polisClientParticipation.git
RUN cd polisClientParticipation && npm install



ENTRYPOINT ["polisClientParticipation/x"]
